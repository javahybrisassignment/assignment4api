package com.nagarro.advjava.app.ws.services;

import java.util.List;

import com.nagarro.advjava.app.ws.model.EmployeeReqModel;

public interface EmployeeService {
	
	public boolean updateEmployee(EmployeeReqModel emp);
	
	public EmployeeReqModel getEmployee(int id);

	public List<EmployeeReqModel> getEmployees();
}
