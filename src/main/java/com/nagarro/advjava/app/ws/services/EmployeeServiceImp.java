package com.nagarro.advjava.app.ws.services;

import java.util.List;

import com.nagarro.advjava.app.ws.db.DatabaseMethods;
import com.nagarro.advjava.app.ws.model.EmployeeReqModel;

public class EmployeeServiceImp implements EmployeeService {

	public boolean updateEmployee(EmployeeReqModel emp) {
		
		boolean res = DatabaseMethods.updateEmployees(emp);
				
		return res;
	}
	
	public EmployeeReqModel getEmployee(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<EmployeeReqModel> getEmployees() {
		
		List<EmployeeReqModel> empList = DatabaseMethods.getEmployees();
		
		return empList;
	}
}
