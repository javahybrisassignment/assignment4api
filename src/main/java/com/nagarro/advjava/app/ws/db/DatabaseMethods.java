package com.nagarro.advjava.app.ws.db;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.BeanUtils;

import com.nagarro.advjava.app.ws.entity.Employee;
import com.nagarro.advjava.app.ws.model.EmployeeReqModel;



public class DatabaseMethods {
	
	
public static List<EmployeeReqModel> getEmployees() {
		
		List <Employee> list = null;
		 List<EmployeeReqModel> returnValue = null;
		//System.out.println("File name :"+file.getAbsolutePath()+" "+file.getName() + file.length());
			
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Employee.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		try {
		
					
		Query query = session.createQuery("from Employee");
        
		list = query.list();
        
        System.out.println("List is : "+ list);
        
        session.getTransaction().commit();
        
        returnValue = new ArrayList<EmployeeReqModel>();
        for (Employee emp : list) {
        	System.out.println(emp.getEmpId());
        	EmployeeReqModel empN = new EmployeeReqModel();
            BeanUtils.copyProperties(emp, empN);
            returnValue.add(empN);
        }
        
					
		} finally {
			//session.flush();
			//session.close();
			//factory.close();
		}	
		return returnValue;
	}

	public static boolean updateEmployees(EmployeeReqModel empReq) {
		
		boolean returnFlag = false;
		//System.out.println("File name :"+file.getAbsolutePath()+" "+file.getName() + file.length());
			
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Employee.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		try {
		
		Employee emp = new Employee();
        BeanUtils.copyProperties(empReq	, emp);	
		
		Query query = session.createQuery("update Employee set empName = :empName," +
											" empAddress = :empAddress,"+
											" empEmail = :empEmail,"+
											" empDOB = :empDOB"+											
											" where empId = :empId");
		query.setParameter("empId", emp.getEmpId());
		query.setParameter("empDOB", emp.getEmpDOB());
		query.setParameter("empName", emp.getEmpName());
		query.setParameter("empEmail", emp.getEmpEmail());
		query.setParameter("empAddress", emp.getEmpAddress());
		
		int result = query.executeUpdate();
	    
		returnFlag = result>0?true:false;
	    
	    session.getTransaction().commit();
	    
					
		} finally {
			//session.flush();
			//session.close();
			//factory.close();
		}	
		return returnFlag;
	}
}
