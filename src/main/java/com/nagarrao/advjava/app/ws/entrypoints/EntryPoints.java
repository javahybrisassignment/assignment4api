package com.nagarrao.advjava.app.ws.entrypoints;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.nagarro.advjava.app.ws.model.EmployeeReqModel;
import com.nagarro.advjava.app.ws.model.EmployeeResModel;
import com.nagarro.advjava.app.ws.services.EmployeeService;
import com.nagarro.advjava.app.ws.services.EmployeeServiceImp;


@Path("/emp")
public class EntryPoints {
	
	//method to list all users
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<EmployeeReqModel> showUsers() {
		
		List<EmployeeReqModel> empList = null;
		
		EmployeeService empService = new EmployeeServiceImp();
		
		empList = empService.getEmployees();
		
		return empList;
	}
	
	//method to update user data
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.APPLICATION_JSON})
	public String updateUser(EmployeeReqModel empModel) {
		String resStr = null;
		
		EmployeeService empService = new EmployeeServiceImp();
		
			System.out.println("API-----(date)>"+empModel.getEmpDOB());
		
		boolean flag = empService.updateEmployee(empModel);
		
		resStr = flag? "true" : "false";
		
		return resStr;
	}
}
